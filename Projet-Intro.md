# Project-Password-Generator

Pour ce projet j'ai réalisé un programme qui permet de régler un souci majeur dans notre société actuelle : les failles des mots de passe.
En effet pour protéger vos informations, il est nécessaire de choisir et d'utiliser des mots de passe robustes, c'est-à-dire difficiles à
retrouver à l'aide d'outils automatisés et à deviner par une tierce personne.
Voici quelques recommandations :
- Utilisez un mot de passe unique pour chaque service. En particulier, l'utilisation d'un même mot de passe entre sa messagerie professionnelle
et sa messagerie personnelle est à proscrire.
- Choisissez un mot de passe qui n'a pas de lien avec vous (mot de passe composé d'un nom de société, d'une date de naissance,etc...).
- Modifiez systématiquement et au plus tôt les mots de passe par défaut lorsque les systèmes en contiennent.
- Renouvelez vos mots de passe avec une fréquence raisonnable.

La robustesse d'un mot de passe dépend en général d'abord de sa complexité, si vous souhaitez une règle très simple : choisissez des mots de passe 
d'au moins 12 caractères de types différents (majuscules, minuscules, chiffres, caractères spéciaux).

Enfin, pour vous évitez cette étape fastidieuse d'imaginer des mots de passe à répétiton, et que ceux-ci soit le plus sécurisé possible,
j'ai imaginé un programme automatique qui permet de concevoir le nombre de mot de passe que l'on souhaite ainsi que le nombre de caractère, le nombre de chiffre,
le nombre de caractère spéciaux, et le dossier dans lequel on veut importer les mots de passe.

Pour plus d'information, cf vidéo explicative ...